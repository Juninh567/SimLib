# Contributing

Always use update(); at the end of your functions so it does not glitch.

Pull requests that modify files other than simlib.js will be only accepted with a reason.

# If you are new to contributing:

1 - Fork this repository

2 - Create a new branch to work on

3 - Make the changes

4 - Open a pull request

5 - If it's accepted your branch will be merged with the main one
