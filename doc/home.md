# SimLib documentation

## Commands

write(text) - Writes text on the screen

clear() - Clears the screen

draw(source) - Draws any kind of image supported by the browser

make_header(content) - Writes a big and bold text on the screen

cache_content() - Caches the page's contents to be restored later

recoverCachedContent()  - Restores the cached content saved after using the cache_content function (currently rendered items are kept)

set_title(new_title) - Changes the page's title

stream(source, parameters) or str(source, parameters) - Streams any kind of video supported by the browser with paramaters. 
The paramaters field is required but can handle spaces

getUsers(value) - Get information from the user, currently "browser" is the only one currently available.

redirect(url) - Redirect user to the specified URL.

createWindow(path, x, y) - Create a window with the specified url,

then resize it to the specified dimensions (Only works when

triggered by the user, for example when pressing a button)

createButton(url, text) - Create a button with specified text,

leading to the specified URL

makeScriptedBtn(jsfunc, text) - Create a button with specified

text, executing the specified function

## Miscellaneous commands

update() - Updates the old_content variable (only needed if you are making your own command)
