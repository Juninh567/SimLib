# Beta

## b1.1 (Patch)

- Fix bugs

## b1.0 (Major)

- Added creating window feature, see line 28 in the [documentation file](doc/home.md)

- Added creating button feature, both URL and scripted.

- Finally out of alpha

# Alpha

## a1.2 (Major)

- Added the cache reset, redirect and get information features.

- Bug fixes

- Smaller minified version

## a1.1 (Minor)

- Moved the documentation to it's own file

- Fixed bug related to the embed feature

## a1.0.1 (Minor)

- Added embed feature, wich allows you to insert another page in your page (iframe).

- Added this changelog.

## a1.0 Patch 1 (Patch)

- Fixed grammar errors in the coded documentation.

## a1.0 (Major)

- First public release of SimLib.
